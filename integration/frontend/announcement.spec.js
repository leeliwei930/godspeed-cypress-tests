/*
 * Created by Li Wei Lee, on 19/4/2020.
 */

describe('Test Announcement', () => {
    let words = cy.faker.lorem.words();
    let paragraph = cy.faker.lorem.paragraph();
    let publishDate = cy.moment().format("M/D/Y");

    let announcementID;

    // it('Login To Backend' , () => {
    //     cy.visit('/backend/backend/auth/signin');
    //     cy.get("input[name='login']").type('admin');
    //     cy.get("input[name='password']").type('admin');
    //     cy.get('button').contains('Login').click();
    //     cy.url().should('include', 'backend');
    //
    // });

    it('Create Announcement', () => {
        cy.visit('/backend/rainlab/blog/posts/create')
        cy.get('div[data-field-name="title"]  input').type(words);
        cy.get('#BlogMarkdown-formContent-content-code > .ace_scroller > .ace_content').type(paragraph);
        cy.get('a[title="Manage"]').click();
        cy.get('div[data-field-name="published"]  label').click();
        cy.get('div[data-field-name="published_at"]  input#DatePicker-formPublishedAt-date-published_at').type(publishDate);
        cy.get('a').contains('Save').click();
        cy.get('textarea[name="Post[content]"]').should('have.text' , paragraph);
        cy.wait(1500)

    });

    it('View Announcements', () => {
        cy.visit('/announcements');
        cy.contains(words).should('be.visible')
        let targetButton = `a[data-cy="announcement-${words}"]`;


        cy.get(targetButton).click();
    });

    it('Check On Announcement', () => {


        announcementID = Cypress.$('div[data-cy="announcement"]').attr('data-cy-announcement-id');
        cy.contains(words).should('be.visible');
    });

    it('Delete Announcement', () => {
        cy.visit(`backend/rainlab/blog/posts/update/${announcementID}`);
        cy.get('button[data-request="onDelete"]').click()
        cy.wait(1500)
        cy.get('button').contains('OK').click();
        cy.wait(1500)

        cy.url().should('equal' , 'http://acceptance-flametree.test/backend/rainlab/blog/posts')

    })

});
describe('Test Announcement Policy', () => {
    let categoryName = cy.faker.lorem.words(6);
    let categorySlug = Cypress._.toLower(categoryName).replace(/ /g,'-')

    let words = cy.faker.lorem.words();
    let paragraph = cy.faker.lorem.paragraph();
    let publishDate = cy.moment().format("M/D/Y");
    it('Create private announcement category', () => {
        cy.visit('/backend/rainlab/blog/categories/create');
        cy.get('input[name="Category[name]"]').type(categoryName);
        cy.get('label[for="Form-field-Category-required_auth"]').click();
        cy.wait(500)
        cy.contains("Only visible to").should('be.visible');
        cy.get('span[aria-labelledby="select2-Form-field-Category-user_group-container"]').click();
        cy.contains("Volunteer").should('be.visible');
        cy.get('.select2-search__field').type('Volunteer{enter}');
        cy.get('button').contains('Create').click();
    });
    it('Redirect to my created category', () => {
        cy.wait(800);
        cy.get('input[name="Category[name]"]').should('have.value', categoryName);
    })

    it('Create announcement', () => {
        cy.get('.is-native-drag > .nav > .active > a').contains('Blog').click();
        cy.get('.btn-primary').contains('New post').click();
        cy.get('#Form-field-Post-title').type(words);
        cy.get('#BlogMarkdown-formContent-content-code > .ace_scroller > .ace_content').type(paragraph);
        cy.get('a').contains('Manage').click();
        cy.get('label').contains('Published').click();
        cy.get('#DatePicker-formPublishedAt-date-published_at').type(publishDate);

        cy.get('a').contains('Save').click();
        cy.get('#Form-field-Post-title').should('have.value' , words);
        cy.wait(1800)
    })

    it('Assign announcement to private category', () => {
        cy.get(':nth-child(2) > a > .title > span').contains('Categories').click();
        cy.get('label').contains(categoryName).click();
        cy.get('a').contains('Save').click();

    });

    it('Try to view announcement without login', () => {
        cy.visit(`/announcement/${categorySlug}`, {failOnStatusCode: false});
    })

    it('Expect 404 Not Found' , () => {
       cy.contains("404").should('be.visible')
    })
});

describe('Test Public Announcement', () => {
    let announcementCategory = cy.faker.lorem.words(6);
    let announcementTitle = cy.faker.lorem.words(6)
    let announcementParagraph = cy.faker.lorem.paragraph()
    let publishDate = cy.moment().format("M/D/Y")

    it("Create public announcement category", () => {
        cy.visit('/backend/rainlab/blog/categories/create');
        cy.get('div[data-field-name="name"] input').type(announcementCategory);
        cy.get('button').contains('Create').click()
        cy.get('div[data-field-name="name"] input').should('have.value', announcementCategory)
    })

    it("Create an announcement and assign to public announcement categories", () => {
        cy.visit('/backend/rainlab/blog/posts/create')
        cy.get('div[data-field-name="title"]  input').type(announcementTitle)
        cy.get('div[data-field-name="content"]  .ace_content').type(announcementParagraph)
        cy.get('a[title="Categories"]').click()
        cy.get('div[data-field-name="categories"]').contains(announcementCategory).click()
        cy.get('a[title="Manage"]').click()
        cy.get('label').contains('Published').click()
        cy.get('input[id="DatePicker-formPublishedAt-date-published_at"]').type(publishDate)
        cy.get('a').contains('Save').click()
        cy.get('div[data-field-name="title"] input').should('have.value', announcementTitle)
    })

    it('Expect to see it on announcements list page' , () => {
        cy.visit('/announcements');
        cy.contains(announcementTitle).should('be.visible');
    })

    it('Expect to see it on announcement page', () => {
        let targetButton = `a[data-cy="announcement-${announcementTitle}"]`
        cy.get(targetButton).click();
        cy.get('h2[class="post-title"]').should('have.html', announcementTitle)
    })
})

describe('Test Private Announcement', () => {
    let announcementCategory = cy.faker.lorem.words(6)
    let announcementTitle = cy.faker.lorem.words(6)
    let announcementParagraph = cy.faker.lorem.paragraph()
    let publishDate = cy.moment().format("M/D/Y")

    it("Create a private announcement category", () => {
        cy.visit('/backend/rainlab/blog/categories/create')
        cy.get('div[data-field-name="name"]  input').type(announcementCategory)
        cy.get('div[data-field-name="required_auth"]').contains('Restricted Access').click()
        cy.contains('Only visible to').should('be.visible')
        cy.get('span[aria-labelledby="select2-Form-field-Category-user_group-container"]').click()
        cy.contains('Volunteer').should('be.visible')
        cy.get('input[class="select2-search__field"]').type('Volunteer{enter}')
        cy.get('button').contains('Create').click()
        cy.get('div[data-field-name="name"] input').should('have.value', announcementCategory)
    })

    it("Create a private announcement and assign to announcement categories", () => {
        cy.visit('/backend/rainlab/blog/posts/create')
        cy.get('div[data-field-name="title"]  input').type(announcementTitle)
        cy.get('div[data-field-name="content"]  .ace_content').type(announcementParagraph)
        cy.get('a[title="Categories"]').click()
        cy.get('div[data-field-name="categories"]').contains(announcementCategory).click()
        cy.get('a[title="Manage"]').click()
        cy.get('label').contains('Published').click()
        cy.get('input[id="DatePicker-formPublishedAt-date-published_at"]').type(publishDate)
        cy.get('a').contains('Save').click()
        cy.get('div[data-field-name="title"] input').should('have.value', announcementTitle)
    })

    it('Try to login with a volunteer account', () => {
        cy.visit('/membership/login')
        cy.get("input[name='login']").type('volunteer@mail.com');
        cy.get("input[name='password']").type('secret123');
        cy.get('button').contains('LOG IN').click();
        cy.url().should('include', 'dashboard');
    })

    it('View private announcement in Announcements page' , () => {
        cy.visit('/membership/announcements')
        cy.visit('/membership/announcements-categories', {failOnStatusCode: false})
        cy.get('h2[class="card-title px-4 py-3"]').should('be.visible', announcementCategory)
    })
})
