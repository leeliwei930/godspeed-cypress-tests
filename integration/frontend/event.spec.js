describe('Create An Event', function () {
    let title = cy.faker.lorem.words(6)
    let description = cy.faker.lorem.words(6)
    let content = cy.faker.lorem.paragraph()
    let titleSlug = Cypress._.toLower(title).replace(/ /g,'-')
    let email = "member@mail.com"

    it('Go to backend and create an event', function () {
        cy.visit("http://acceptance-flametree.test/backend/godspeed/flametreecms/events", {failOnStatusCode: false})
        cy.get("a").contains("New Event").click()
        cy.url().should("equal", "http://acceptance-flametree.test/backend/godspeed/flametreecms/events/create")
        cy.get('div[data-field-name="title"]').type(title)
        cy.get('div[data-field-name="description"]').type(description)
        cy.get('input[id="DatePicker-formStartedAt-time-started_at"]').clear({force: true}).should("have.value", "").type("18:00", {force: true})
        cy.get('input[id="DatePicker-formEndedAt-time-ended_at"]').clear({force: true}).should("have.value", "").type("23:00", {force: true})
        cy.get('span[id="select2-Form-field-Event-timezone-container"]').type("Australia/Sydney{enter}")
        cy.get('div[id="RichEditor-formContentHtml-content_html"]').type(content)
        cy.get('div[id="Relation-formUserGroup-user_group"]').contains("Member").click()
        cy.get('button').contains("Create").click()
        cy.get('div[data-field-name="title"]').should("be.visible", title)
    });

    it('Login with a member account', () => {
        cy.visit('http://acceptance-flametree.test/membership/login')
        cy.get('input[name="login"]').type(email)
        cy.get('input[name="password"]').type('secret123')
        cy.get('button').contains('LOG IN').click();
        cy.url().should('equal', 'http://acceptance-flametree.test/membership/dashboard')
        cy.get('p').should('be.visible', email)
    })

    it('Go to EVENTS page', () => {
        cy.visit('http://acceptance-flametree.test/membership/events', {failOnStatusCode: false})
        cy.get('.event-title').should("have.html", title)
        cy.get('a').contains("MORE DETAILS").click()
        cy.url().should('equal', `http://acceptance-flametree.test/membership/event/${titleSlug}`)
        cy.get('h3[class="post-title"]').should("have.html", title)
    })
});

describe('Edit an event', () => {
    let title = cy.faker.lorem.words(6)
    let description = cy.faker.lorem.words(6)
    let content = cy.faker.lorem.paragraph()
    let newTitle = "New Title"

    it('Go to backend and create an event', function () {
        cy.visit("http://acceptance-flametree.test/backend/godspeed/flametreecms/events", {failOnStatusCode: false})
        cy.get("a").contains("New Event").click()
        cy.url().should("equal", "http://acceptance-flametree.test/backend/godspeed/flametreecms/events/create")
        cy.get('div[data-field-name="title"]').type(title)
        cy.get('div[data-field-name="description"]').type(description)
        cy.get('input[id="DatePicker-formStartedAt-time-started_at"]').clear({force: true}).should("have.value", "").type("18:00", {force: true})
        cy.get('input[id="DatePicker-formEndedAt-time-ended_at"]').clear({force: true}).should("have.value", "").type("23:00", {force: true})
        cy.get('span[id="select2-Form-field-Event-timezone-container"]').type("Australia/Sydney{enter}")
        cy.get('div[id="RichEditor-formContentHtml-content_html"]').type(content)
        cy.get('div[id="Relation-formUserGroup-user_group"]').contains("Member").click()
        cy.get('button').contains("Create").click()
        cy.get('div[data-field-name="title"]').should("be.visible", title)
    })

    it('Edit an event', function () {
        cy.visit("http://acceptance-flametree.test/backend/godspeed/flametreecms/events", {failOnStatusCode: false})
        cy.get('tr.rowlink').contains(title).click()
        cy.get('div[data-field-name="title"]').find("input").clear().should("have.value", "").type(newTitle)
        cy.get('button').contains("Save").click()
        cy.get('div[data-field-name="title"]').should("be.visible", newTitle)
        cy.visit("http://acceptance-flametree.test/backend/godspeed/flametreecms/events", {failOnStatusCode: false})
        cy.get('tr.rowlink').contains(newTitle).should("be.visible", newTitle)
    });
})


