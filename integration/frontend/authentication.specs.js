describe('Test On Signing Up and Sign out when referral code exists', () => {
    let name = "John Doe"
    let email = "johndoe@mail.com"
    let password = "secret123"
    let referralCode = "referral-code12345"
    let timezone = "Australia/Sydney"
    let past = cy.moment().subtract(10, 'days').format("M/D/Y").toString()
    let future = cy.moment().add(10, 'days').format("M/D/Y").toString()

    it('Create a legit new referral', () => {
        cy.visit('/backend/godspeed/flametreecms/referrals', {failOnStatusCode: false})
        cy.get('a').contains('New Referral').click()
        cy.url().should('equal', 'http://acceptance-flametree.test/backend/godspeed/flametreecms/referrals/create')
        cy.get('input[id="Form-field-Referral-code"]').type(referralCode)
        cy.get('span[aria-labelledby="select2-Form-field-Referral-timezone-container"]').click()
        cy.contains(timezone).should('be.visible')
        cy.get('input[class="select2-search__field"]').type(timezone + "{enter}")
        cy.get('input[id="DatePicker-formValidBefore-date-valid_before"]').type(future)
        cy.get('input[id="Form-field-Referral-capped"]').click()
        cy.get('input[id="DatePicker-formValidAfter-date-valid_after"]').type(past)
        cy.contains('Maximum Redemption').should('be.visible')
        cy.get('input[id="Form-field-Referral-usage_left"').type("1", {force: true})
        cy.contains('Only Available To').should('be.visible')
        cy.get('div[id="Form-field-Referral-user_group-group"').click()
        cy.contains('Volunteer').should('be.visible')
        cy.get('input[class="select2-search__field"]').type('Volunteer{enter}')
        cy.get('button').contains('Create').click()
        cy.get('input[id="Form-field-Referral-code"]').should('have.value', referralCode)
    })

    it('Sign up with a referral code that exists', () => {
        cy.visit('/membership/login', {failOnStatusCode: false})
        cy.get('a').contains('Create a new account').click()
        cy.url().should('equal', 'http://acceptance-flametree.test/membership/signup')
        cy.get('input[id="name"]').type(name)
        cy.get('input[id="email"]').type(email)
        cy.get('input[id="new_password"]').type(password)
        cy.get('input[id="new_password_confirmation"]').type(password)
        cy.get('input[id="referral_code"]').type(referralCode)
        cy.get('button').contains('SIGN UP').click()
    })

    it('Expects to be logged in', () => {
        cy.url().should('equal', 'http://acceptance-flametree.test/membership/dashboard')
        cy.get('p').should('be.visible', email)
    })

    it('Try to sign user out', () => {
        cy.get('div[class="dropdown-card "]').click()
        cy.get('a[class="dropdown-card-menu-item"]').contains("Log Out").click({force: true})
        cy.url().should('equal', 'http://acceptance-flametree.test/membership/login')
    })

    it('Check if the referral code has been used by the created user', () => {
        cy.visit('/backend/godspeed/flametreecms/referrals', {failOnStatusCode: false})
        cy.get('div[id="Lists"]').should('be.visible', referralCode).click()
    })
})

describe('Test On Signing Up and Sign out when referral code does not exist', () => {
    let name = "John Doe"
    let email = "johndoe@mail.com"
    let password = "secret123"
    let referralCode = "referralcode12345"
    let timezone = "Australia/Sydney"
    let past = cy.moment().add(10, 'days').format("M/D/Y").toString()
    let future = cy.moment().subtract(10, 'days').format("M/D/Y").toString()

    it('Create a wrong new referral', () => {
        cy.visit('/backend/godspeed/flametreecms/referrals', {failOnStatusCode: false})
        cy.get('a').contains('New Referral').click()
        cy.url().should('equal', 'http://acceptance-flametree.test/backend/godspeed/flametreecms/referrals/create')
        cy.get('input[id="Form-field-Referral-code"]').type(referralCode)
        cy.get('span[aria-labelledby="select2-Form-field-Referral-timezone-container"]').click()
        cy.contains(timezone).should('be.visible')
        cy.get('input[class="select2-search__field"]').type(timezone + "{enter}")
        cy.get('input[id="DatePicker-formValidBefore-date-valid_before"]').type(future)
        cy.get('input[id="Form-field-Referral-capped"]').click()
        cy.get('input[id="DatePicker-formValidAfter-date-valid_after"]').type(past)
        cy.contains('Maximum Redemption').should('be.visible')
        cy.get('input[id="Form-field-Referral-usage_left"').type("1", {force: true})
        cy.contains('Only Available To').should('be.visible')
        cy.get('div[id="Form-field-Referral-user_group-group"').click()
        cy.contains('Volunteer').should('be.visible')
        cy.get('input[class="select2-search__field"]').type('Volunteer{enter}')
        cy.get('button').contains('Create').click()
        cy.get('input[id="Form-field-Referral-code"]').should('have.value', referralCode)
    })

    it('Go to Login page and sign up with a referral code that does not exist', () => {
        cy.visit('/membership/login', {failOnStatusCode: false})
        cy.get('a').contains('Create a new account').click()
        cy.url().should('equal', 'http://acceptance-flametree.test/membership/signup')
        cy.get('input[id="name"]').type(name)
        cy.get('input[id="email"]').type(email)
        cy.get('input[id="new_password"]').type(password)
        cy.get('input[id="new_password_confirmation"]').type(password)
        cy.get('input[id="referral_code"]').type(referralCode)
        cy.get('button').contains('SIGN UP').click()
    })

    it('Expects to be on the same page because referral code does not exist', () => {
        cy.url().should('equal', 'http://acceptance-flametree.test/membership/signup')
    })
})
