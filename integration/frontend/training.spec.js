describe('Test On Trainings', () => {
    let email = 'member@mail.com'
    let trainingTitle = cy.faker.lorem.words(6)
    let url = trainingTitle.replace(/\s+/g, '-').toLowerCase()
    let trainingContent = cy.faker.lorem.words(10)

    it('Create a new training', () => {
        cy.visit('/backend/godspeed/flametreecms/trainings')
        cy.get('a').contains('New Training').click()
        cy.get('div[data-field-name="title"] input').type(trainingTitle)
        cy.get('div[data-field-name="content_html"]').type(trainingContent)
        cy.get('div[class="field-checkboxlist "]').contains('Member').click()
        cy.get('button').contains('Create').click()
        cy.get('div[data-field-name="title"] input').should('have.value', trainingTitle)
    })

    it('Login with a member account', () => {
        cy.visit('membership/login')
        cy.get('input[name="login"]').type(email)
        cy.get('input[name="password"]').type('secret123')
        cy.get('button').contains('LOG IN').click();
        cy.url().should('equal', 'http://acceptance-flametree.test/membership/dashboard')
        cy.get('p').should('be.visible', email)
    })

    it('Member expects to see and click the training resource that is created.', () => {
        let link = `http://acceptance-flametree.test/membership/training/${url}`
        let buttonLink = `a[href="${link}"]`
        cy.visit('/membership/training-resources', {failOnStatusCode: false})
        cy.get(buttonLink).click()
        cy.url().should('equal', link)
        cy.get('h3[class="post-title"]').should('have.html', trainingTitle)
    })
})
