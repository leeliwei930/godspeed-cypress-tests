describe("Edit User Profile In FrontEnd", () => {
    let email = "member@mail.com"
    let oldPassword = "secret123"
    let newPassword = "secret1234"

    it("Login and change password in frontend.", () => {
        cy.visit("http://acceptance-flametree.test/membership/login")
        cy.url().should('equal', "http://acceptance-flametree.test/membership/login")
        cy.get('div[class="flametree-theme"]').should("be.visible", "PORTAL LOGIN")
        cy.get('input[id="volunteer_login_email"]').type(email)
        cy.get('input[id="volunteer_login_password"]').type(oldPassword)
        cy.get('button').contains("LOG IN").click()
        cy.url().should('equal', 'http://acceptance-flametree.test/membership/dashboard')
        cy.get('p').should('be.visible', "member@mail.com")
        cy.visit("http://acceptance-flametree.test/membership/profile-settings")
        cy.get('input[id="email"]').should("be.visible", email)
        cy.get('input[id="reset_password"]').click()
        cy.get('input[id="phone_number"]').type("1234567")
        cy.get('input[id="current_password"]').type(oldPassword)
        cy.get('input[id="new_password"]').type(newPassword)
        cy.get('input[id="new_password_confirmation"]').type(newPassword)
        cy.get('button').contains("Save Changes").click()
    })

    // logout and login with a new password
    it('Try to sign user out', () => {
        cy.get('div[class="dropdown-card "]').click()
        cy.get('a[class="dropdown-card-menu-item"]').contains("Log Out").click({force: true})
        cy.url().should('equal', 'http://acceptance-flametree.test/membership/login')
    })

    it("Login and change password in frontend.", () => {
        cy.visit("http://acceptance-flametree.test/membership/login")
        cy.url().should('equal', "http://acceptance-flametree.test/membership/login")
        cy.get('div[class="flametree-theme"]').should("be.visible", "PORTAL LOGIN")
        cy.get('input[id="volunteer_login_email"]').type(email)
        cy.get('input[id="volunteer_login_password"]').type(newPassword)
        cy.get('button').contains("LOG IN").click()
        cy.url().should('equal', 'http://acceptance-flametree.test/membership/dashboard')
        cy.get('p').should('be.visible', email)
        cy.wait(5000)
    })
})
